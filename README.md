This project will be way to make todo lists, incorporate them into a calendar
and sync them with a server.

MAKE TODO LISTS
  create dataclass that stores the following info
    Name, Category, Desc, Due date, Recurring (T/F), Period of reccurence
  Store that data - Most likely an SQLite database
  Pull it out in human readable format

INCORPORATE A CALENDAR
  Figure out a CLI calendar that is clickable - unsure if this is possible
    maybe do a gui interface if the CLI doesn't work

SYNC WITH SERVER
  Most likely sync with the thinkpad server when on local network.
  have that server push storage file to github daily.
  create pull command

INCORPORATE INTO QTILE
  Figure out how to make pop up windows on bar click
  add to bar
  create secondary bar?
  Output TODO list and calendar to imagemagick to write to whatever wall paper
    I'm using.
  Update xinitrc to account for the updated file
